Feature: User Management



  Background:
    Given a user called simon with administrator permissions and password S@feb3ar
    And a user called tom with risk_management permissions and password S@feb3ar


    @high-impact
    Scenario Outline: The administrator checks a user's details
      When simon is logged in with password <password>
      Then he is able to view <user>'s account
      Examples:
        | password | user |
        | S@feb3ar | tom  |

    @high-risk
    @to-do
    Scenario Outline: A user's password is reset by the administrator
      Given a <user> has lost her password
      When simon is logged in with password S@feb3ar
      Then simon can reset <user>'s password
      Examples:
        | user |
        | tom |
